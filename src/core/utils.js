module.exports = {
  getResponsiveTag: function () {
    var tag = window.getComputedStyle(document.body, ':before').getPropertyValue('content')
    tag = tag.replace(/"|'/g, '')
    return tag
  }
}
