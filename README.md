# vizexplorer

> VizExplorer Assignment

# Dependencies
* node v4.4.5
* npm 2.15.5



## install dependencies
```npm install```


## serve with hot reload at localhost:8080
```npm run dev```


## build for production with minification
```npm run build```
